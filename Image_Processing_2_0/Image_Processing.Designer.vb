﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Image_Processing
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btn_Start = New System.Windows.Forms.Button()
        Me.ProgressBar_All = New System.Windows.Forms.ProgressBar()
        Me.ProcessingStatus_All = New System.Windows.Forms.Label()
        Me.LastUpdate_File = New System.Windows.Forms.Label()
        Me.LastUpdate_DateTime = New System.Windows.Forms.Label()
        Me.rdl_Interface_FA = New System.Windows.Forms.RadioButton()
        Me.rdl_Interface_ADM = New System.Windows.Forms.RadioButton()
        Me.Grbx_Interface = New System.Windows.Forms.GroupBox()
        Me.Grbx_Team = New System.Windows.Forms.GroupBox()
        Me.rdl_Team_PMP = New System.Windows.Forms.RadioButton()
        Me.rdl_Team_EP = New System.Windows.Forms.RadioButton()
        Me.ProgressBar_Current = New System.Windows.Forms.ProgressBar()
        Me.ProcessingStatus_Current = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Pic_Problem_Status = New System.Windows.Forms.PictureBox()
        Me.Pic_Duplicate_Status = New System.Windows.Forms.PictureBox()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lblDuplicate_Folder = New System.Windows.Forms.Label()
        Me.lblProblem_Folder = New System.Windows.Forms.Label()
        Me.LblDatabase_Folder = New System.Windows.Forms.Label()
        Me.lblMain_Folder = New System.Windows.Forms.Label()
        Me.lblVersion = New System.Windows.Forms.Label()
        Me.Grbx_Interface.SuspendLayout()
        Me.Grbx_Team.SuspendLayout()
        CType(Me.Pic_Problem_Status, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Pic_Duplicate_Status, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btn_Start
        '
        Me.btn_Start.Location = New System.Drawing.Point(63, 140)
        Me.btn_Start.Name = "btn_Start"
        Me.btn_Start.Size = New System.Drawing.Size(138, 67)
        Me.btn_Start.TabIndex = 0
        Me.btn_Start.Text = "Start Processing"
        Me.btn_Start.UseVisualStyleBackColor = True
        '
        'ProgressBar_All
        '
        Me.ProgressBar_All.Location = New System.Drawing.Point(22, 24)
        Me.ProgressBar_All.Name = "ProgressBar_All"
        Me.ProgressBar_All.Size = New System.Drawing.Size(237, 20)
        Me.ProgressBar_All.TabIndex = 1
        '
        'ProcessingStatus_All
        '
        Me.ProcessingStatus_All.AutoSize = True
        Me.ProcessingStatus_All.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ProcessingStatus_All.Location = New System.Drawing.Point(12, 9)
        Me.ProcessingStatus_All.Name = "ProcessingStatus_All"
        Me.ProcessingStatus_All.Size = New System.Drawing.Size(0, 13)
        Me.ProcessingStatus_All.TabIndex = 2
        '
        'LastUpdate_File
        '
        Me.LastUpdate_File.AutoSize = True
        Me.LastUpdate_File.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LastUpdate_File.Location = New System.Drawing.Point(3, 0)
        Me.LastUpdate_File.Name = "LastUpdate_File"
        Me.LastUpdate_File.Size = New System.Drawing.Size(0, 13)
        Me.LastUpdate_File.TabIndex = 3
        '
        'LastUpdate_DateTime
        '
        Me.LastUpdate_DateTime.AutoSize = True
        Me.LastUpdate_DateTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LastUpdate_DateTime.Location = New System.Drawing.Point(12, 121)
        Me.LastUpdate_DateTime.Name = "LastUpdate_DateTime"
        Me.LastUpdate_DateTime.Size = New System.Drawing.Size(0, 13)
        Me.LastUpdate_DateTime.TabIndex = 4
        '
        'rdl_Interface_FA
        '
        Me.rdl_Interface_FA.AutoSize = True
        Me.rdl_Interface_FA.Location = New System.Drawing.Point(20, 19)
        Me.rdl_Interface_FA.Name = "rdl_Interface_FA"
        Me.rdl_Interface_FA.Size = New System.Drawing.Size(85, 17)
        Me.rdl_Interface_FA.TabIndex = 6
        Me.rdl_Interface_FA.Text = "Financial Aid"
        Me.rdl_Interface_FA.UseVisualStyleBackColor = True
        '
        'rdl_Interface_ADM
        '
        Me.rdl_Interface_ADM.AutoSize = True
        Me.rdl_Interface_ADM.Checked = True
        Me.rdl_Interface_ADM.Location = New System.Drawing.Point(20, 42)
        Me.rdl_Interface_ADM.Name = "rdl_Interface_ADM"
        Me.rdl_Interface_ADM.Size = New System.Drawing.Size(77, 17)
        Me.rdl_Interface_ADM.TabIndex = 7
        Me.rdl_Interface_ADM.TabStop = True
        Me.rdl_Interface_ADM.Text = "Admissions"
        Me.rdl_Interface_ADM.UseVisualStyleBackColor = True
        '
        'Grbx_Interface
        '
        Me.Grbx_Interface.Controls.Add(Me.rdl_Interface_FA)
        Me.Grbx_Interface.Controls.Add(Me.rdl_Interface_ADM)
        Me.Grbx_Interface.Location = New System.Drawing.Point(9, 216)
        Me.Grbx_Interface.Name = "Grbx_Interface"
        Me.Grbx_Interface.Size = New System.Drawing.Size(125, 70)
        Me.Grbx_Interface.TabIndex = 8
        Me.Grbx_Interface.TabStop = False
        Me.Grbx_Interface.Text = "Interface"
        '
        'Grbx_Team
        '
        Me.Grbx_Team.Controls.Add(Me.rdl_Team_PMP)
        Me.Grbx_Team.Controls.Add(Me.rdl_Team_EP)
        Me.Grbx_Team.Location = New System.Drawing.Point(147, 216)
        Me.Grbx_Team.Name = "Grbx_Team"
        Me.Grbx_Team.Size = New System.Drawing.Size(125, 70)
        Me.Grbx_Team.TabIndex = 10
        Me.Grbx_Team.TabStop = False
        Me.Grbx_Team.Text = "Team"
        '
        'rdl_Team_PMP
        '
        Me.rdl_Team_PMP.AutoSize = True
        Me.rdl_Team_PMP.Checked = True
        Me.rdl_Team_PMP.Location = New System.Drawing.Point(20, 19)
        Me.rdl_Team_PMP.Name = "rdl_Team_PMP"
        Me.rdl_Team_PMP.Size = New System.Drawing.Size(48, 17)
        Me.rdl_Team_PMP.TabIndex = 6
        Me.rdl_Team_PMP.TabStop = True
        Me.rdl_Team_PMP.Text = "PMP"
        Me.rdl_Team_PMP.UseVisualStyleBackColor = True
        '
        'rdl_Team_EP
        '
        Me.rdl_Team_EP.AutoSize = True
        Me.rdl_Team_EP.Location = New System.Drawing.Point(20, 42)
        Me.rdl_Team_EP.Name = "rdl_Team_EP"
        Me.rdl_Team_EP.Size = New System.Drawing.Size(39, 17)
        Me.rdl_Team_EP.TabIndex = 7
        Me.rdl_Team_EP.Text = "EP"
        Me.rdl_Team_EP.UseVisualStyleBackColor = True
        '
        'ProgressBar_Current
        '
        Me.ProgressBar_Current.Location = New System.Drawing.Point(22, 64)
        Me.ProgressBar_Current.Name = "ProgressBar_Current"
        Me.ProgressBar_Current.Size = New System.Drawing.Size(237, 20)
        Me.ProgressBar_Current.TabIndex = 11
        '
        'ProcessingStatus_Current
        '
        Me.ProcessingStatus_Current.AutoSize = True
        Me.ProcessingStatus_Current.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ProcessingStatus_Current.Location = New System.Drawing.Point(12, 50)
        Me.ProcessingStatus_Current.Name = "ProcessingStatus_Current"
        Me.ProcessingStatus_Current.Size = New System.Drawing.Size(0, 13)
        Me.ProcessingStatus_Current.TabIndex = 12
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(23, 294)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(148, 17)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "Problem Folder Status"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(24, 321)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(155, 17)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Duplicate Folder Status"
        '
        'Pic_Problem_Status
        '
        Me.Pic_Problem_Status.Image = Global.Image_Processing_2_0.My.Resources.Resources.green_bug
        Me.Pic_Problem_Status.Location = New System.Drawing.Point(172, 293)
        Me.Pic_Problem_Status.Name = "Pic_Problem_Status"
        Me.Pic_Problem_Status.Size = New System.Drawing.Size(22, 22)
        Me.Pic_Problem_Status.TabIndex = 15
        Me.Pic_Problem_Status.TabStop = False
        '
        'Pic_Duplicate_Status
        '
        Me.Pic_Duplicate_Status.Image = Global.Image_Processing_2_0.My.Resources.Resources.green_bug
        Me.Pic_Duplicate_Status.Location = New System.Drawing.Point(179, 321)
        Me.Pic_Duplicate_Status.Name = "Pic_Duplicate_Status"
        Me.Pic_Duplicate_Status.Size = New System.Drawing.Size(22, 22)
        Me.Pic_Duplicate_Status.TabIndex = 16
        Me.Pic_Duplicate_Status.TabStop = False
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.LastUpdate_File)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(6, 90)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(272, 30)
        Me.FlowLayoutPanel1.TabIndex = 20
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(23, 373)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(62, 13)
        Me.Label3.TabIndex = 21
        Me.Label3.Text = "Main Folder"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(23, 396)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(85, 13)
        Me.Label4.TabIndex = 22
        Me.Label4.Text = "Database Folder"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(23, 420)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(77, 13)
        Me.Label5.TabIndex = 23
        Me.Label5.Text = "Problem Folder"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(23, 441)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(84, 13)
        Me.Label6.TabIndex = 24
        Me.Label6.Text = "Duplicate Folder"
        '
        'lblDuplicate_Folder
        '
        Me.lblDuplicate_Folder.AutoSize = True
        Me.lblDuplicate_Folder.Location = New System.Drawing.Point(114, 441)
        Me.lblDuplicate_Folder.Name = "lblDuplicate_Folder"
        Me.lblDuplicate_Folder.Size = New System.Drawing.Size(0, 13)
        Me.lblDuplicate_Folder.TabIndex = 28
        '
        'lblProblem_Folder
        '
        Me.lblProblem_Folder.AutoSize = True
        Me.lblProblem_Folder.Location = New System.Drawing.Point(114, 420)
        Me.lblProblem_Folder.Name = "lblProblem_Folder"
        Me.lblProblem_Folder.Size = New System.Drawing.Size(0, 13)
        Me.lblProblem_Folder.TabIndex = 27
        '
        'LblDatabase_Folder
        '
        Me.LblDatabase_Folder.AutoSize = True
        Me.LblDatabase_Folder.Location = New System.Drawing.Point(114, 396)
        Me.LblDatabase_Folder.Name = "LblDatabase_Folder"
        Me.LblDatabase_Folder.Size = New System.Drawing.Size(0, 13)
        Me.LblDatabase_Folder.TabIndex = 26
        '
        'lblMain_Folder
        '
        Me.lblMain_Folder.AutoSize = True
        Me.lblMain_Folder.Location = New System.Drawing.Point(114, 373)
        Me.lblMain_Folder.Name = "lblMain_Folder"
        Me.lblMain_Folder.Size = New System.Drawing.Size(0, 13)
        Me.lblMain_Folder.TabIndex = 25
        '
        'lblVersion
        '
        Me.lblVersion.AutoSize = True
        Me.lblVersion.Font = New System.Drawing.Font("Microsoft Sans Serif", 5.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVersion.Location = New System.Drawing.Point(9, 345)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(0, 7)
        Me.lblVersion.TabIndex = 61
        '
        'Image_Processing
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 481)
        Me.Controls.Add(Me.lblVersion)
        Me.Controls.Add(Me.lblDuplicate_Folder)
        Me.Controls.Add(Me.lblProblem_Folder)
        Me.Controls.Add(Me.LblDatabase_Folder)
        Me.Controls.Add(Me.lblMain_Folder)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.Pic_Duplicate_Status)
        Me.Controls.Add(Me.Pic_Problem_Status)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ProcessingStatus_Current)
        Me.Controls.Add(Me.ProgressBar_Current)
        Me.Controls.Add(Me.Grbx_Team)
        Me.Controls.Add(Me.Grbx_Interface)
        Me.Controls.Add(Me.LastUpdate_DateTime)
        Me.Controls.Add(Me.ProcessingStatus_All)
        Me.Controls.Add(Me.ProgressBar_All)
        Me.Controls.Add(Me.btn_Start)
        Me.Name = "Image_Processing"
        Me.Text = "Image_Processing"
        Me.Grbx_Interface.ResumeLayout(False)
        Me.Grbx_Interface.PerformLayout()
        Me.Grbx_Team.ResumeLayout(False)
        Me.Grbx_Team.PerformLayout()
        CType(Me.Pic_Problem_Status, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Pic_Duplicate_Status, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btn_Start As System.Windows.Forms.Button
    Friend WithEvents ProgressBar_All As System.Windows.Forms.ProgressBar
    Friend WithEvents ProcessingStatus_All As System.Windows.Forms.Label
    Friend WithEvents LastUpdate_File As System.Windows.Forms.Label
    Friend WithEvents LastUpdate_DateTime As System.Windows.Forms.Label
    Friend WithEvents rdl_Interface_FA As System.Windows.Forms.RadioButton
    Friend WithEvents rdl_Interface_ADM As System.Windows.Forms.RadioButton
    Friend WithEvents Grbx_Interface As System.Windows.Forms.GroupBox
    Friend WithEvents Grbx_Team As System.Windows.Forms.GroupBox
    Friend WithEvents rdl_Team_PMP As System.Windows.Forms.RadioButton
    Friend WithEvents rdl_Team_EP As System.Windows.Forms.RadioButton
    Friend WithEvents ProgressBar_Current As System.Windows.Forms.ProgressBar
    Friend WithEvents ProcessingStatus_Current As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Pic_Problem_Status As System.Windows.Forms.PictureBox
    Friend WithEvents Pic_Duplicate_Status As System.Windows.Forms.PictureBox
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblDuplicate_Folder As System.Windows.Forms.Label
    Friend WithEvents lblProblem_Folder As System.Windows.Forms.Label
    Friend WithEvents LblDatabase_Folder As System.Windows.Forms.Label
    Friend WithEvents lblMain_Folder As System.Windows.Forms.Label
    Friend WithEvents lblVersion As System.Windows.Forms.Label

End Class
