﻿Public Class FilenameFilter
    Protected _condition As FilterType
    Protected _searchPattern As String

    Public Sub New()

    End Sub

    Public Sub New(ByVal condition As FilterType, ByVal searchPattern As String)
        Me.Condition = condition
        Me.SearchPattern = searchPattern
    End Sub

    Public Property Condition() As FilterType
        Get
            Return _condition
        End Get
        Set(ByVal value As FilterType)
            _condition = value
        End Set
    End Property

    Public Property SearchPattern() As String
        Get
            Return _searchPattern
        End Get
        Set(ByVal value As String)
            _searchPattern = value
        End Set
    End Property
End Class
