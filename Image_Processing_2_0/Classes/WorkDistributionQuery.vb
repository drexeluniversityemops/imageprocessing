﻿Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Threading.Thread
Imports System.Net.Mail

Public Class WorkDistributionQuery
    Inherits FileQuery

    Public Sub New(ByVal searchDirectory As String)
        MyBase.New(searchDirectory)
    End Sub

    Public Sub New(ByVal searchDirectory As String, ByVal extension As String)
        MyBase.New(searchDirectory, extension)
    End Sub

    Public Overrides Function PerformQuery() As List(Of FileInfo)
        Return DoWorkQuery()
    End Function

    Protected Function DoWorkQuery() As List(Of FileInfo)
        Dim fileInfos As New List(Of FileInfo) 'we return this list

        'Need to make sure the file is at least 30 seconds old
        Dim DateTime_Now_30_Seconds As DateTime = DateAdd(DateInterval.Second, -30, DateTime.Now())

        'build the query
        Dim allFiles = From file In FastDirectoryEnumerator.GetFiles(Me.SearchDirectory, Me.FileExtensionFilter, SearchOption.TopDirectoryOnly) _
                       Where file.LastAccessTimeUtc.ToLocalTime < DateTime_Now_30_Seconds _
                       Order By file.Name.Substring(0, 1) Ascending _
                       Select file

        'continue building the query by adding filters (if any)
        For Each filt In _filters
            Dim searchPattern As String = filt.SearchPattern
            If (filt.Condition = FilterType.Contains) Then
                allFiles = From f In allFiles _
                        Where f.Name.Contains(searchPattern) _
                        Select f
            ElseIf (filt.Condition = FilterType.NotContains) Then
                allFiles = From f In allFiles _
                        Where Not f.Name.Contains(searchPattern) _
                        Select f
            End If
        Next

        Dim fileList As FileData()

        fileList = FastDirectoryEnumerator.GetFiles(Me.SearchDirectory, Me.FileExtensionFilter, SearchOption.TopDirectoryOnly)

        'start the linq query from scratch using fileList
        allFiles = From f In fileList _
                    Where f.LastAccessTimeUtc.ToLocalTime < DateTime_Now_30_Seconds _
                    Order By f.Name Ascending _
                    Select f

        For Each fd As FileData In allFiles
            Dim filename = Path.GetFullPath(fd.Path)
            fileInfos.Add(New FileInfo(filename))
        Next

        If allFiles.Count > 0 Then
            fileInfos = GetFamilyOfFiles(allFiles)
        End If

        Return fileInfos
    End Function

    Public Function GetFamilyOfFiles(ByVal allFiles As IEnumerable(Of FileData)) As List(Of FileInfo)
        'Check to make sure filename is in a valid format
        'We should have the following:
        '(3) '-'
        '(2) '_'
        'Starts with an integer
        'Is 40 characters in length

        '        If Len(allFiles.First().Name) = "40" And CountCharacter(allFiles.First().Name, "-") = 3 And CountCharacter(allFiles.First().Name, "_") = 2 And FirstCharIntCheck(allFiles.First().Name) Then

        'Check the database to see if the file is already in database.  If so then move to error folder.

        'When allFiles.First().Name the Linq query will be run
        Dim familyName As String = Microsoft.VisualBasic.Strings.Mid(allFiles.First().Name, 8, 13)

        'just retrieve the list of files that contain the familyName in its filename
        Dim filter = "*" & familyName & Me.FileExtensionFilter

        'Dim filter = "*" & Me.FileExtensionFilter

        Dim familyOfFiles = FastDirectoryEnumerator.GetFiles(Me.SearchDirectory, filter, SearchOption.TopDirectoryOnly)

        'return a list of FileInfos
        Dim list As New List(Of FileInfo)
        For Each fd As FileData In familyOfFiles
            Dim filename = Path.Combine(Me.SearchDirectory, fd.Name)
            list.Add(New FileInfo(fd.Path))
        Next
        Return list
    End Function


    ' Move one file from one folder to another folder.
    Public Shared Sub MoveOneFile(ByVal fName As String, ByVal DestinationPath As String, ByVal Duplicate_Folder As String)
        If (Not Directory.Exists(DestinationPath)) Then
            MessageBox.Show(String.Format("The Directory '{0}' does not exist.", DestinationPath))
        Else
            'MessageBox.Show(File.GetLastAccessTime(fName))

TryMoveAgain:
            If File.Exists(fName) Then
                Dim dFile As String = String.Empty

                dFile = Path.GetFileName(fName)
                'Ignore the file if it is the Thumbs.db file
                If dFile <> "Thumbs.db" Then
                    Dim dFilePath As String = String.Empty
                    dFilePath = Path.Combine(DestinationPath, dFile)

                    'File_Available(fName, 0.025)
                    'Make sure destination ends with a "\" if not throw error
                    If Microsoft.VisualBasic.Right(DestinationPath, 1) <> "\" Then
                        MessageBox.Show("ERROR: The path to the destination is not valid.  It must end in a \. File has not been moved")
                    ElseIf Microsoft.VisualBasic.Right(Duplicate_Folder, 1) <> "\" Then
                        MessageBox.Show("ERROR: The path to the destination is not valid.  It must end in a \. File has not been moved")
                    Else
                        'Check to make sure file is not already in destination location if it is move it to the DestinationPath_Duplicate folder
                        If File.Exists(dFilePath) Then
                            'Sending mail not permitted on PCI network
                            'Send_Email("IMAGE PROCESSING - There was a problem on " & SystemInformation.ComputerName, "Check on " & SystemInformation.ComputerName & " there was a problem at " & DateTime.Now & ". File name already in folder-" & fName & ". Added timestamp to file name")
                            'File.Move(fName, Duplicate_Folder + DateTime.Now.ToString("yyyyMMddhhmmss") + "_" + dFile)
                            File.Move(fName, DestinationPath + DateTime.Now.ToString("yyyyMMddhhmmss") + "_" + dFile)
                            Exit Sub
                        End If

                        Dim isProcessed As Boolean = False
                        Do While Not isProcessed
                            Try
                                File.Move(fName, DestinationPath + dFile)
                            Catch ex As Exception
                                isProcessed = False
                            Finally
                                isProcessed = True
                            End Try
                        Loop

                        'Check to see if file is still in the original location.  If so, move it again.
                        If File.Exists(fName) Then
                            GoTo TryMoveAgain
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Function File_Available(ByVal sFileName As String, ByVal WaitTime As String)
        Dim dt As DateTime = File.GetLastAccessTime(sFileName)
        Do While DateTime.Now.Subtract(dt).TotalMinutes < WaitTime
            dt = File.GetLastAccessTime(sFileName)
            Sleep(100)
        Loop
        Return 1
    End Function

    Public Function CountCharacter(ByVal value As String, ByVal ch As Char) As Integer
        Return value.Count(Function(c As Char) c = ch)
    End Function
    Public Function FirstCharIntCheck(ByVal value As String) As Boolean
        Dim result As Integer
        Dim append As Boolean = True
        If Not Int32.TryParse(value.Substring(0, 1), result) Then
            'Not an integer 
            append = False
        End If
        Return append
    End Function

    Protected Sub Dispose(ByVal disposing As Boolean)
        Throw New NotImplementedException
    End Sub
    Public Shared Sub chkCategory(ByVal EM_WD_Category_Desc As String)
        Dim tmp As New ArrayList()

        'Return "True"
    End Sub

    'Sending mail not permitted on PCI network
    'Public Shared Sub Send_Email(ByVal Msg_Subject As String, _
    '                     ByVal Msg_Text As String)
    '    If System.Configuration.ConfigurationManager.AppSettings("Send_Email") = True Then
    '        Dim client As New SmtpClient()
    '        client.DeliveryMethod = SmtpDeliveryMethod.Network
    '        client.EnableSsl = True
    '        client.Host = "smtp.gmail.com"
    '        client.Port = 587

    '        ' setup Smtp authentication   
    '        Dim credentials As New System.Net.NetworkCredential("emoperation@gmail.com", "u&^%$#()")
    '        client.UseDefaultCredentials = False
    '        client.Credentials = credentials

    '        Dim msg As New MailMessage()
    '        msg.From = New MailAddress("emoperation@gmail.com")
    '        msg.To.Add(New MailAddress("dc78@drexel.edu"))
    '        msg.To.Add(New MailAddress("twh22@drexel.edu"))
    '        msg.To.Add(New MailAddress("rsm82@drexel.edu"))
    '        msg.To.Add(New MailAddress("bkl27@drexel.edu"))
    '        'msg.To.Add(New MailAddress("spg23@drexel.edu"))
    '        'msg.To.Add(New MailAddress("emf42@drexel.edu"))
    '        'msg.To.Add(New MailAddress("drl58@drexel.edu"))
    '        'msg.To.Add(New MailAddress("mjw347@drexel.edu"))

    '        'Definite()
    '        'You – dc78 – unless you’d like to be excluded
    '        'Me – twh22
    '        'Steph – spg23
    '        'Trish – pm54

    '        'Could be value in their knowing
    '        'Earlene – emf42
    '        'Shirley – sos23 if she is here at night it might be helpful for her to know the system is down and that is why images are not coming through
    '        'Bertha – bkl27 just worried she tries to fix something

    '        'Maybe – more from a standpoint of warning you if Steph and I were out, but, if you are getting the email you will know already
    '        'Hameed – ha58
    '        'Kathy – km438
    '        'Ed – sykesed

    '        msg.Subject = Msg_Subject
    '        msg.IsBodyHtml = True
    '        msg.Body = String.Format(Msg_Text)

    '        Try
    '            client.Send(msg)
    '            'lblMsg.Text = "Your message has been successfully sent."
    '        Catch ex As Exception
    '            'lblMsg.ForeColor = Color.Red
    '            'lblMsg.Text = "Error occured while sending your message." + ex.Message
    '        End Try
    '    End If

    'End Sub
End Class
