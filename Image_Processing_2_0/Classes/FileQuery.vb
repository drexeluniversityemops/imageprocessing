﻿Imports System.IO
Imports System.Text.RegularExpressions

Public MustInherit Class FileQuery


    Protected _dir As String
    Protected _ext As String

    Protected _filters As New List(Of FilenameFilter)

    Public Sub New(ByVal searchDirectory As String)
        _dir = searchDirectory
        _ext = "*.*"
    End Sub

    Public Sub New(ByVal searchDirectory As String, ByVal extension As String)
        _dir = searchDirectory
        _ext = extension
    End Sub

    Public MustOverride Function PerformQuery() As List(Of FileInfo)

    Public Overridable Property SearchDirectory() As String
        Get
            Return _dir
        End Get
        Set(ByVal value As String)
            _dir = value
        End Set
    End Property

    Public Overridable Property FileExtensionFilter() As String
        Get
            Return _ext
        End Get
        Set(ByVal value As String)
            _ext = value
        End Set
    End Property



    Public Overridable Property FilenameFilters() As List(Of FilenameFilter)
        Get
            Return _filters
        End Get
        Set(ByVal value As List(Of FilenameFilter))
            _filters = value
        End Set
    End Property

    Public Overridable Sub AddFilter(ByVal filter As FilenameFilter)
        _filters.Add(filter)
    End Sub
End Class
