﻿Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Data.SqlClient
Imports System.Data.SqlTypes
Imports System
Imports System.Linq
Imports System.Windows.Forms
Imports System.Threading.Thread
Imports System.Threading
Imports System.ComponentModel



Public Class Image_Processing
    Dim OutputString As String = Nothing
    Dim files As List(Of FileInfo)

    Dim Last_Processed_File As String
    Dim Last_Processed_DateTime As String

    Dim Main_File_Folder As String
    Dim DB_Folder As String
    Dim Problem_Folder As String
    Dim Duplicate_Folder As String
    Dim EM_WD_Interface_ID As Integer

    Dim Duplicate_Folder_Status As Boolean
    Dim Problem_Folder_Status As Boolean

    Private Delegate Sub UI_Update_Folder_Delegate(ByVal Status As String)
    Delegate Sub Update_UI_AllProgressDelegate(ByVal Status As String, ByVal totalDigits As Integer, ByVal digitsSoFar As Integer, <System.Runtime.InteropServices.Out()> ByRef cancel As Boolean)
    Delegate Sub Update_UI_CurrentProgressDelegate(ByVal Status As String, ByVal totalDigits As Integer, ByVal digitsSoFar As Integer, <System.Runtime.InteropServices.Out()> ByRef cancel As Boolean)
    Private Delegate Sub Process_Delegate(ByVal digits As Integer)
    Dim myConnection As SqlConnection

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Check to see if the form is active or the version is the same
        If Not Form_Active() Then
            MessageBox.Show("The current form is disabled or you are using an older version.")

            'Disable the start button
            btn_Start.Enabled = False

            'Close the form
            Me.DialogResult = DialogResult.Cancel
            Exit Sub
        End If

        'Output the version to a label on the form
        lblVersion.Text = Application.ProductVersion

    End Sub

    Private Function Form_Active() As Boolean
        Dim bStatus As Boolean = False

        Try
            'Get the form version from the database
            Dim dtForm_Check As DataTable
            Dim TA_Form_Check As New dsCheck_Form_StatusTableAdapters.EM_WD_Check_Form_StatusTableAdapter
            dtForm_Check = TA_Form_Check.GetData_Form_Status(Application.ProductName, _
                                                             Application.ProductVersion)
            If dtForm_Check.Rows.Count = 0 Then
                'There are no forms with the current version.  
                bStatus = False
            Else
                bStatus = dtForm_Check.Rows(0).Item(0).ToString
            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
        Return bStatus

    End Function

    ' Move files from one folder to another folder.
    Public Sub GetFiles(ByVal sourcePath As String)
        Dim query As FileQuery

        If (Not Directory.Exists(sourcePath)) Then
            MessageBox.Show(String.Format("The Directory '{0}' does not exist.", sourcePath))
            Return
        End If

        query = New WorkDistributionQuery(sourcePath, System.Configuration.ConfigurationManager.AppSettings("File_Type"))

        'Run the query and get back the files it found
        files = query.PerformQuery()

        If files Is Nothing Then
            'There are no files to process or there is a problem with the filename
            'MessageBox.Show("There are no files to process.")
        Else

            Dim Total_File_Count As Integer = files.Count
            Dim Current_File_Count As Integer = 1

            For Each fInfo In files

                'Show the status of the file being processed
                ShowProgress_All("Processing file " & Current_File_Count & " of " & Total_File_Count, Total_File_Count, Current_File_Count, False)
                ShowProgress_Current("Processing file " & fInfo.Name.ToString, 100, 20, False)

                If Len(fInfo.Name.ToString) = "40" And CountCharacter(fInfo.Name.ToString, "-") = 3 And CountCharacter(fInfo.Name.ToString, "_") = 2 And FirstCharIntCheck(fInfo.Name.ToString) Then
                    Dim Priority As String
                    Dim Category As String
                    Dim Source As String
                    Dim Image_Family As String
                    Dim Banner_ID As Object
                    Dim Image_Family_Count As Integer
                    Dim Index As String

                    Priority = Microsoft.VisualBasic.Strings.Mid(fInfo.Name.ToString, 1, 1)
                    Category = Microsoft.VisualBasic.Strings.Mid(fInfo.Name.ToString, 3, 2)
                    Source = Microsoft.VisualBasic.Strings.Mid(fInfo.Name.ToString, 6, 2)

                    Image_Family = Microsoft.VisualBasic.Strings.Mid(fInfo.Name.ToString, 8, 13)
                    If Microsoft.VisualBasic.Strings.Mid(fInfo.Name.ToString, 22, 8) = "00000000" Then
                        Banner_ID = System.DBNull.Value
                    Else
                        Banner_ID = Microsoft.VisualBasic.Strings.Mid(fInfo.Name.ToString, 22, 8)
                    End If
                    Image_Family_Count = Microsoft.VisualBasic.Strings.Mid(fInfo.Name.ToString, 31, 3)
                    Index = Microsoft.VisualBasic.Strings.Mid(fInfo.Name.ToString, 35, 2)

                    'Capture information about file and insert into database
                    CaptureFileInfo(fInfo, sourcePath, files.Count, Priority, Category, Source, Image_Family, Banner_ID, Image_Family_Count, Index)
                Else

                    'Show status updated
                    ShowProgress_Current("Error: Filename problem for " & fInfo.Name.ToString & " moving file.", 100, 100, False)

                    'If there is a problem in the filename return nothing and move file to problem folder
                    'Sending mail not permitted on PCI network
                    'WorkDistributionQuery.Send_Email("IMAGE PROCESSING - There was a problem on " & SystemInformation.ComputerName, "Check on " & SystemInformation.ComputerName & " there was a problem at " & DateTime.Now & ". Filename not properly formated-" & fInfo.FullName.ToString & ". Moved to problem folder")
                    WorkDistributionQuery.MoveOneFile(fInfo.FullName.ToString, Problem_Folder, Duplicate_Folder)

                    'Show status updated
                    ShowProgress_Current("Error: Filename problem for " & fInfo.Name.ToString & " file moved.", 100, 100, False)

                    Last_Processed_File = "Last processed file had an error (filename): " & fInfo.Name.ToString
                    Last_Processed_DateTime = " at " & DateTime.Now
                End If

                Current_File_Count = Current_File_Count + 1

                'Update the status for the problem and duplicate folders
                Duplicate_Folder_Status = chk_for_Empty_Folder(Duplicate_Folder)
                Problem_Folder_Status = chk_for_Empty_Folder(Problem_Folder)
                UI_Update_Folder()
            Next
        End If
    End Sub

    Public Sub CaptureFileInfo(ByVal fInfo As Object, ByVal sourcePath As String, ByVal FamilyCount As String, ByVal Priority As String, ByVal Category As String, ByVal Source As String, ByVal Image_Family As String, ByVal Banner_ID As Object, ByVal Image_Family_Count As String, ByVal Index As String)

        ShowProgress_Current("Capturing file information for " & fInfo.Name.ToString, 100, 40, False)

        'Need to determine date is older, the created date or the modified date to be used for selecting the oldest date.
        Dim File_Modified_Date As DateTime = fInfo.LastWriteTime
        Dim File_Created_Date As DateTime = fInfo.CreationTime
        Dim Oldest_Date As DateTime = fInfo.CreationTime

        'Set the default queue to general
        Dim Image_Queue_Type_ID As Integer = 1

        'Allow the data queue only for Admission documents
        If EM_WD_Interface_ID = 2 Then
            'The default queue for EP items is Queue_Type_ID = 10
            If rdl_Team_EP.Checked Then
                Image_Queue_Type_ID = 10
            End If

            'Documents are no longer being sent to DataBank.
            'If Priority = 5 Then
            '    'Set the queue type to send to Databank (ID = 6)
            '    Image_Queue_Type_ID = 6
            'ElseIf Priority = 6 Then
            '    'Set the queue type to send to Databank (ID = 6)
            '    Image_Queue_Type_ID = 6
            'ElseIf Priority = 7 Then
            '    'Set the queue type to send to Databank (ID = 6)
            '    Image_Queue_Type_ID = 6
            'ElseIf Priority = 8 Then
            '    'Set the queue type to send to Databank (ID = 6)
            '    Image_Queue_Type_ID = 6
            'ElseIf Priority = 9 Then
            '    'Set the queue type to send to Databank (ID = 6)
            '    Image_Queue_Type_ID = 6
            'End If
        End If

        'If the created date is greater or equal to the file modified date, use the modified date (it is older)
        If File_Created_Date > File_Modified_Date Then
            Oldest_Date = fInfo.LastWriteTime
        End If

        ShowProgress_Current("Loading file information for " & fInfo.Name.ToString, 100, 60, False)

        Try
            'If rdl_DB_Prod.Checked Then
            myConnection = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Image_Processing_2_0.My.MySettings.EM_Image_Processing").ToString())
            'Else
            'myConnection = New SqlConnection("Server=sql-2005-dev.drexel.edu\dev,2980;uid=emDevwebw;pwd=Wajyu6cp98rd;database=EM_Image_Processing")
            'myConnection = New SqlConnection("Server=SQL-2005-TEST.DREXEL.EDU\TEST,3291;uid=emDevwebw;pwd=Wajyu6cp98rd;database=EM_Image_Processing")
            'End If
            Dim myCommand As New SqlCommand
            myCommand.Connection = myConnection
            myCommand.CommandText = "dbo.EM_WD_Image_Processing_Image_ins"

            ' Mark the Command as a SPROC 
            myCommand.CommandType = CommandType.StoredProcedure

            Dim Image_Queue_Team_ID As Object = System.DBNull.Value
            Dim Image_Queue_Users_ID As Object = System.DBNull.Value
            Dim Image_Queues_Added_Users_ID As Integer = 1
            Dim Image_Queues_Assigned_Datetime As Object = System.DBNull.Value
            Dim Image_Queues_Assigned_Users_ID As Object = System.DBNull.Value
            Dim EM_WD_Users_ID As Integer = 1

            If rdl_Team_EP.Checked Then
                EM_WD_Users_ID = 22
                Image_Queue_Team_ID = 8
                Image_Queue_Users_ID = 22
                Image_Queues_Added_Users_ID = 22
                Image_Queues_Assigned_Datetime = DateTime.Now
                Image_Queues_Assigned_Users_ID = 22
            End If
            myCommand.Parameters.AddWithValue("@EM_WD_Interface_ID", EM_WD_Interface_ID)
            myCommand.Parameters.AddWithValue("@Banner_ID", Banner_ID)
            myCommand.Parameters.AddWithValue("@Image_Filename", fInfo.Name.ToString)
            myCommand.Parameters.AddWithValue("@Image_Full_Path", DB_Folder & fInfo.Name.ToString)
            myCommand.Parameters.AddWithValue("@Image_Directory", DB_Folder)
            myCommand.Parameters.AddWithValue("@Image_Family_Count", Image_Family_Count)
            myCommand.Parameters.AddWithValue("@Image_Family_Total_Count", FamilyCount.ToString)
            myCommand.Parameters.AddWithValue("@Image_Created_Datetime", fInfo.CreationTime)
            myCommand.Parameters.AddWithValue("@Image_Modified_Datetime", fInfo.LastWriteTime)
            myCommand.Parameters.AddWithValue("@Image_Size", fInfo.Length.ToString)
            myCommand.Parameters.AddWithValue("@Image_Add_Datetime", DateTime.Now)
            myCommand.Parameters.AddWithValue("@EM_WD_Priority_Code", Priority)
            myCommand.Parameters.AddWithValue("@EM_WD_Category_Code", Category)
            myCommand.Parameters.AddWithValue("@EM_WD_Index_Code", Index)
            myCommand.Parameters.AddWithValue("@EM_WD_Source_Code", Source)
            myCommand.Parameters.AddWithValue("@Image_Family", Image_Family)
            myCommand.Parameters.AddWithValue("@Image_Oldest_Datetime", Oldest_Date)
            myCommand.Parameters.AddWithValue("@EM_WD_Users_ID", EM_WD_Users_ID)
            myCommand.Parameters.AddWithValue("@Image_Queue_Type_ID", Image_Queue_Type_ID)
            'myCommand.Parameters.AddWithValue("@Image_Queue_Type_ID", 88)
            myCommand.Parameters.AddWithValue("@Image_Status_ID", 1)
            myCommand.Parameters.AddWithValue("@Image_Queue_Team_ID", Image_Queue_Team_ID)
            myCommand.Parameters.AddWithValue("@Image_Queue_Users_ID", Image_Queue_Users_ID)
            myCommand.Parameters.AddWithValue("@Image_Queues_Added_Users_ID", Image_Queues_Added_Users_ID)
            myCommand.Parameters.AddWithValue("@Image_Queues_Assigned_Datetime", Image_Queues_Assigned_Datetime)
            myCommand.Parameters.AddWithValue("@Image_Queues_Assigned_Users_ID", Image_Queues_Assigned_Users_ID)
            myCommand.Parameters.AddWithValue("@Image_Problem_Type_ID", 0)


            Dim Error_MessageParameter As New SqlParameter("@Error_Message", SqlDbType.VarChar, 150)
            Error_MessageParameter.Direction = ParameterDirection.Output
            myCommand.Parameters.Add(Error_MessageParameter)

            myConnection.Open()
            myCommand.ExecuteNonQuery()
            Dim Error_Message = Error_MessageParameter.Value

            'Move file from main folder to error folder
            If Error_Message = "" Then
                'Show status updated
                ShowProgress_Current("File loaded.  Moving file:" & fInfo.Name.ToString, 100, 80, False)

                WorkDistributionQuery.MoveOneFile(fInfo.FullName, DB_Folder, Duplicate_Folder)

                'Show status updated
                ShowProgress_Current("File moved: " & fInfo.Name.ToString, 100, 100, False)

                Last_Processed_File = "Last processed file: " & fInfo.Name.ToString
                Last_Processed_DateTime = " at " & DateTime.Now

            ElseIf Error_Message = "File Already In Database" Then

                'Show status updated
                ShowProgress_Current("Error: File in database for " & fInfo.Name.ToString & " moving file.", 100, 80, False)

                'Move the files to the duplicate folder
                'Sending mail not permitted on PCI network
                'WorkDistributionQuery.Send_Email("IMAGE PROCESSING - There was a problem on " & SystemInformation.ComputerName, "Check on " & SystemInformation.ComputerName & " there was a problem at " & DateTime.Now & ". File name already in database-" & fInfo.FullName & " Moved to duplicate folder.")
                WorkDistributionQuery.MoveOneFile(fInfo.FullName, Duplicate_Folder, Duplicate_Folder)

                'Show status updated
                ShowProgress_Current("Error: File in database for " & fInfo.Name.ToString & " file moved.", 100, 100, False)

                Last_Processed_File = "Last processed file had an error (already in database): " & fInfo.Name.ToString
                Last_Processed_DateTime = " at " & DateTime.Now
            Else

                'Show status updated
                ShowProgress_Current("Error: Coding problem for " & fInfo.Name.ToString & " moving file.", 100, 80, False)

                'Move the files to the error folder
                'Sending mail not permitted on PCI network
                'WorkDistributionQuery.Send_Email("IMAGE PROCESSING - There was a problem on " & SystemInformation.ComputerName, "Check on " & SystemInformation.ComputerName & " there was a problem at " & DateTime.Now & ". The file " & fInfo.FullName & " has the following problems:" & Error_Message & ". File moved to problem folder.")
                WorkDistributionQuery.MoveOneFile(fInfo.FullName, Problem_Folder, Duplicate_Folder)

                'Show status updated
                ShowProgress_Current("Error: Coding problem for " & fInfo.Name.ToString & " file moved.", 100, 100, False)

                Last_Processed_File = "Last processed file had an error (coding error): " & fInfo.Name.ToString
                Last_Processed_DateTime = " at " & DateTime.Now
            End If
        Catch ex As Exception

            'Show status updated
            ShowProgress_Current("Error: Coding problem for " & fInfo.Name.ToString & " moving file.", 100, 80, False)

            'Sending mail not permitted on PCI network
            'WorkDistributionQuery.Send_Email("IMAGE PROCESSING - There was a problem on " & SystemInformation.ComputerName, "Check on " & SystemInformation.ComputerName & " there was a problem at " & DateTime.Now & ". The file " & fInfo.FullName & " has the following problems:" & ex.ToString & ". Check computer for error message.")
            MessageBox.Show(ex.ToString)

            'Show status updated
            ShowProgress_Current("Error: Coding problem for " & fInfo.Name.ToString & " file moved.", 100, 100, False)

            Last_Processed_File = "Last processed file had an error (coding error): " & fInfo.Name.ToString
            Last_Processed_DateTime = " at " & DateTime.Now

        Finally
            myConnection.Close()
            myConnection.Dispose()
            myConnection = Nothing
        End Try

    End Sub

    Private Sub btn_Start_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Start.Click

        'disable the button after it is pushed
        btn_Start.Enabled = False
        rdl_Interface_ADM.Enabled = False
        rdl_Interface_FA.Enabled = False
        rdl_Team_EP.Enabled = False
        rdl_Team_PMP.Enabled = False

        'Set the team for the error folder
        Dim Team As String
        If rdl_Team_PMP.Checked Then
            Team = rdl_Team_PMP.Text
        Else
            Team = rdl_Team_EP.Text
        End If

        If rdl_Interface_FA.Checked Then
            EM_WD_Interface_ID = 1
        Else
            EM_WD_Interface_ID = 2
        End If

        'Setup the User ID
        Dim dtFolder_Locations As DataTable
        Dim TA_Folder_Locations As New dsFolder_LocationsTableAdapters.EM_WD_GetFoldersTableAdapter
        dtFolder_Locations = TA_Folder_Locations.GetData_Folder_Locations(EM_WD_Interface_ID, Application.ProductName, Team)

        If dtFolder_Locations.Rows.Count = 0 Then
            'There are no users with that ID.  Disable Feed Me button
            MessageBox.Show("There was a problem setting up the folders for the form.")
        Else
            If Not IsDBNull(dtFolder_Locations.Rows(0).Item("Main_Folder")) Then
                Main_File_Folder = dtFolder_Locations.Rows(0).Item("Main_Folder").ToString
            Else
                'Disable the start button
                btn_Start.Enabled = False

                MessageBox.Show("There was a problem setting up the Main_Folder for the form.")
                Exit Sub
            End If
            If Not IsDBNull(dtFolder_Locations.Rows(0).Item("DB_Folder")) Then
                DB_Folder = dtFolder_Locations.Rows(0).Item("DB_Folder").ToString
            Else
                'Disable the start button
                btn_Start.Enabled = False

                MessageBox.Show("There was a problem setting up the DB_Folder for the form.")
                Exit Sub
            End If
            If Not IsDBNull(dtFolder_Locations.Rows(0).Item("Problem_Folder")) Then
                Problem_Folder = dtFolder_Locations.Rows(0).Item("Problem_Folder").ToString
            Else
                'Disable the start button
                btn_Start.Enabled = False

                MessageBox.Show("There was a problem setting up the Problem_Folder for the form.")
                Exit Sub
            End If
            If Not IsDBNull(dtFolder_Locations.Rows(0).Item("Duplicate_Folder")) Then
                Duplicate_Folder = dtFolder_Locations.Rows(0).Item("Duplicate_Folder").ToString
            Else
                'Disable the start button
                btn_Start.Enabled = False

                MessageBox.Show("There was a problem setting up the Duplicate_Folder for the form.")
                Exit Sub
            End If
        End If


        'If rdl_DB_Prod.Checked Then
        '    myConnection = New SqlConnection("Server=sql-2005-w1.drexel.edu\w1,3564;uid=emDevwebw;pwd=Wajyu6cp98rd;database=EM_Image_Processing")
        '    If EM_WD_Interface_ID = 1 Then
        '        Main_File_Folder = System.Configuration.ConfigurationManager.AppSettings("Main_File_Folder_FA_PROD")
        '        DB_Folder = System.Configuration.ConfigurationManager.AppSettings("DB_Folder_FA_PROD")
        '        Problem_Folder = System.Configuration.ConfigurationManager.AppSettings("Error_Folder_FA_PROD") & Team & "\problem\"
        '        Duplicate_Folder = System.Configuration.ConfigurationManager.AppSettings("Error_Folder_FA_PROD") & Team & "\duplicate\"
        '    Else
        '        Main_File_Folder = System.Configuration.ConfigurationManager.AppSettings("Main_File_Folder_ADM_PROD")
        '        DB_Folder = System.Configuration.ConfigurationManager.AppSettings("DB_Folder_ADM_PROD")
        '        Problem_Folder = System.Configuration.ConfigurationManager.AppSettings("Error_Folder_ADM_PROD") & Team & "\problem\"
        '        Duplicate_Folder = System.Configuration.ConfigurationManager.AppSettings("Error_Folder_ADM_PROD") & Team & "\duplicate\"
        '    End If
        'Else
        '    myConnection = New SqlConnection("Server=SQL-2005-TEST.DREXEL.EDU\TEST,3291;uid=emDevwebw;pwd=Wajyu6cp98rd;database=EM_Image_Processing")
        '    'myConnection = New SqlConnection("Server=sql-2005-dev.drexel.edu\dev,2980;uid=emDevwebw;pwd=Wajyu6cp98rd;database=EM_Image_Processing")
        '    If EM_WD_Interface_ID = 1 Then
        '        Main_File_Folder = System.Configuration.ConfigurationManager.AppSettings("Main_File_Folder_FA_TEST")
        '        DB_Folder = System.Configuration.ConfigurationManager.AppSettings("DB_Folder_FA_TEST")
        '        Problem_Folder = System.Configuration.ConfigurationManager.AppSettings("Error_Folder_FA_TEST") & Team & "\problem\"
        '        Duplicate_Folder = System.Configuration.ConfigurationManager.AppSettings("Error_Folder_FA_TEST") & Team & "\duplicate\"
        '    Else
        '        Main_File_Folder = System.Configuration.ConfigurationManager.AppSettings("Main_File_Folder_ADM_TEST")
        '        DB_Folder = System.Configuration.ConfigurationManager.AppSettings("DB_Folder_ADM_TEST")
        '        Problem_Folder = System.Configuration.ConfigurationManager.AppSettings("Error_Folder_ADM_TEST") & Team & "\problem\"
        '        Duplicate_Folder = System.Configuration.ConfigurationManager.AppSettings("Error_Folder_ADM_TEST") & Team & "\duplicate\"
        '    End If
        'End If

        'Set the values in the form for reference
        lblMain_Folder.Text = Main_File_Folder
        LblDatabase_Folder.Text = DB_Folder
        lblProblem_Folder.Text = Problem_Folder
        lblDuplicate_Folder.Text = Duplicate_Folder

        'Start background process which assigns work to folders
        Dim Process_Files As New Process_Delegate(AddressOf Me.Distribute_Work)
        Process_Files.BeginInvoke(2, Nothing, Nothing)

        Dim doUI_Update_All As Update_UI_AllProgressDelegate = New Update_UI_AllProgressDelegate(AddressOf Me.ShowProgress_All)

        Dim doUI_Update_Current As Update_UI_CurrentProgressDelegate = New Update_UI_CurrentProgressDelegate(AddressOf Me.ShowProgress_Current)

        Dim doUI_Update_Folder As New UI_Update_Folder_Delegate(AddressOf Me.UI_Update_Folder)
        'Clipboard.Clear()

        'Check the problem and duplicate folders to see if they are empty
        Duplicate_Folder_Status = chk_for_Empty_Folder(Duplicate_Folder)
        Problem_Folder_Status = chk_for_Empty_Folder(Problem_Folder)
        UI_Update_Folder()

        'RenameAllFiles("D:\VS_Projects\DP_Work_Distribution_New\problem", "Supp_Doc", "SuppDoc")

    End Sub


    Private Sub Distribute_Work()
Restart:
        While chk_for_Available_File(Main_File_Folder) = Nothing
            For i = 1 To 30
                'Waiting 30 seconds for next file check
                ShowProgress_All("Processing next file in " & 30 - i & " seconds", 30, i, False)
                ShowProgress_Current("Waiting for file", 100, 0, False)
                Sleep(1000)
            Next

            'Update the status for the problem and duplicate folders
            Duplicate_Folder_Status = chk_for_Empty_Folder(Duplicate_Folder)
            Problem_Folder_Status = chk_for_Empty_Folder(Problem_Folder)
            UI_Update_Folder()
        End While

        'Process available files
        GetFiles(Main_File_Folder)

        GoTo Restart
    End Sub

    Sub ShowProgress_All(ByVal Status As String, _
                     ByVal totalDigits As Integer, _
                     ByVal digitsSoFar As Integer, _
                     <System.Runtime.InteropServices.Out()> ByRef cancel As Boolean)
        ' Make sure we're on the right thread 
        If ProcessingStatus_All.InvokeRequired = False Then
            ProgressBar_All.Maximum = totalDigits
            ProgressBar_All.Value = digitsSoFar
            ProcessingStatus_All.Text = Status
            LastUpdate_File.Text = Last_Processed_File
            LastUpdate_DateTime.Text = Last_Processed_DateTime
        Else
            Dim Update_UI_All As Update_UI_AllProgressDelegate = New Update_UI_AllProgressDelegate(AddressOf Me.ShowProgress_All)
            Dim inoutCancel As Object = False ' Avoid boxing and losing our return value

            ' Show progress synchronously (so we can check for cancel)
            Invoke(Update_UI_All, New Object() {Status, totalDigits, digitsSoFar, inoutCancel})
            cancel = CBool(inoutCancel)
        End If
    End Sub

    Sub ShowProgress_Current(ByVal Status As String, _
                 ByVal totalDigits As Integer, _
                 ByVal digitsSoFar As Integer, _
                 <System.Runtime.InteropServices.Out()> ByRef cancel As Boolean)
        ' Make sure we're on the right thread 
        If ProcessingStatus_All.InvokeRequired = False Then
            ProgressBar_Current.Maximum = totalDigits
            ProgressBar_Current.Value = digitsSoFar
            ProcessingStatus_Current.Text = Status
        Else
            Dim Update_UI_Current As Update_UI_CurrentProgressDelegate = New Update_UI_CurrentProgressDelegate(AddressOf Me.ShowProgress_Current)
            Dim inoutCancel As Object = False ' Avoid boxing and losing our return value

            ' Show progress synchronously (so we can check for cancel)
            Invoke(Update_UI_Current, New Object() {Status, totalDigits, digitsSoFar, inoutCancel})
            cancel = CBool(inoutCancel)
        End If
    End Sub

    Private Sub RenameAllFiles(ByVal path As String, ByVal StringSearch As String, ByVal ReplaceString As String)
        Dim dir As New DirectoryInfo(path)
        Dim oFiles() As FileInfo = dir.GetFiles()
        Dim dirDown() As DirectoryInfo = dir.GetDirectories()
        Dim properEval As MatchEvaluator = New MatchEvaluator(AddressOf properCase)
        Dim lowerEval As MatchEvaluator = New MatchEvaluator(AddressOf lowerCase)
        For Each f As FileInfo In oFiles
            Dim oldName = f.FullName
            Dim newName = Microsoft.VisualBasic.Right(IO.Path.GetFileNameWithoutExtension(f.Name), (Len(IO.Path.GetFileNameWithoutExtension(f.Name)) - 2))

            'Dim newName = Regex.Replace(IO.Path.GetFileNameWithoutExtension(f.Name), StringSearch, ReplaceString, RegexOptions.IgnoreCase)
            newName = f.Directory.ToString() & "\" & newName & f.Extension.ToLower
            Rename(oldName, newName)
        Next
    End Sub

    Public Function properCase(ByVal m As Match) As String
        Return m.Groups(1).ToString().ToUpper & m.Groups(2).ToString().ToLower
    End Function

    Public Function lowerCase(ByVal m As Match) As String
        Return m.Groups(0).ToString().ToLower
    End Function

    Function chk_for_Available_File(ByVal chkDirectory As String)
        Try
            'Check to see if the hold folder and the input folder are empty
            Dim di As DirectoryInfo = New DirectoryInfo(chkDirectory)
            If di.GetFiles(System.Configuration.ConfigurationManager.AppSettings("File_Type")).Length > 0 Then
                Dim fiArr As FileInfo() = di.GetFiles(System.Configuration.ConfigurationManager.AppSettings("File_Type"))
                Dim fri As FileInfo
                For Each fri In fiArr
                    If fri.LastAccessTimeUtc.ToLocalTime < DateAdd(DateInterval.Second, -30, DateTime.Now()) Then
                        Return fri.FullName
                    End If
                Next
                Return Nothing
            Else
                'The folder is empty
                Return Nothing
            End If
        Catch ex As Exception
            Console.WriteLine("The process failed: {0}", ex.ToString())
            Return Nothing
        End Try
    End Function

    Public Function CountCharacter(ByVal value As String, ByVal ch As Char) As Integer
        Return value.Count(Function(c As Char) c = ch)
    End Function

    Public Function FirstCharIntCheck(ByVal value As String) As Boolean
        Dim result As Integer
        Dim append As Boolean = True
        If Not Int32.TryParse(value.Substring(0, 1), result) Then
            'Not an integer 
            append = False
        End If
        Return append
    End Function

    'This is the target subroutine that will be used for the delegate for watching the error folder
    Private Sub UI_Update_Folder()
        If Problem_Folder_Status Then
            Pic_Problem_Status.Image = Image_Processing_2_0.My.Resources.Resources.green_bug
        Else
            Pic_Problem_Status.Image = Image_Processing_2_0.My.Resources.Resources.red_bug
        End If
        If Duplicate_Folder_Status Then
            Pic_Duplicate_Status.Image = Image_Processing_2_0.My.Resources.Resources.green_bug
        Else
            Pic_Duplicate_Status.Image = Image_Processing_2_0.My.Resources.Resources.red_bug
        End If
    End Sub

    Function chk_for_Empty_Folder(ByVal chkDirectory As String) As Boolean
        Dim Folder_Empty As Boolean = True
        Try
            'Check to see if the hold folder and the input folder are empty
            Dim di As DirectoryInfo = New DirectoryInfo(chkDirectory)

            If di.GetFiles("*.pdf").Length > 0 Then
                Folder_Empty = False
            End If
        Catch ex As Exception
            Console.WriteLine("The process failed: {0}", ex.ToString())
        End Try

        Return Folder_Empty
    End Function
End Class
