USE [EM_Image_Processing]
GO
/****** Object:  StoredProcedure [dbo].[EM_WD_Image_Processing_Image_ins]    Script Date: 5/13/2013 4:01:40 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO



ALTER proc [dbo].[EM_WD_Image_Processing_Image_ins]


@EM_WD_Interface_ID int,
@Banner_ID varchar(8),
@Image_Filename varchar(200),
@Image_Full_Path varchar(200),
@Image_Directory varchar(200),
@Image_Family_Count int,
@Image_Family_Total_Count int,
@Image_Created_Datetime datetime,
@Image_Modified_Datetime datetime,
@Image_Size int,
@Image_Add_Datetime datetime,
@EM_WD_Priority_Code varchar(2),
@EM_WD_Category_Code varchar(2),
@EM_WD_Source_Code varchar(2),
@EM_WD_Index_Code varchar(2),
@Image_Family varchar(16),
@Image_Oldest_Datetime datetime,
@EM_WD_Users_ID varchar(50),
@Image_Queue_Type_ID int,
@Image_Status_ID int,
@Image_Queue_Team_ID int,
@Image_Queue_Users_ID int,
@Image_Queues_Added_Users_ID int,
@Image_Queues_Assigned_Datetime datetime,
@Image_Queues_Assigned_Users_ID int,
@Image_Problem_Type_ID int,
@Error_Message varchar(150) OUTPUT

as

DECLARE @Image_ID int
DECLARE @Image_Queues_ID int
SET @Error_Message = ''

/* Check to see if Source Code is in database */
SELECT 
		EM_WD_Source_Code
FROM
		EM_WD_Source
INNER JOIN
		EM_WD_Interface_Source_Lookup
	ON
			EM_WD_Interface_Source_Lookup.EM_WD_Source_ID = EM_WD_Source.EM_WD_Source_ID
WHERE 
		EM_WD_Source_Code = @EM_WD_Source_Code	
	AND
		EM_WD_Source_Display = 1
	AND
		EM_WD_Interface_ID = @EM_WD_Interface_ID
IF @@RowCount = 0
	SET @Error_Message = ', Invalid Source Code'

/* Check to see if Priority Code is in database */
SELECT 
		EM_WD_Priority_Code
FROM
		EM_WD_Priority
INNER JOIN
		EM_WD_Interface_Priority_Lookup
	ON
			EM_WD_Interface_Priority_Lookup.EM_WD_Priority_ID = EM_WD_Priority.EM_WD_Priority_ID
WHERE 
		EM_WD_Priority_Code = @EM_WD_Priority_Code
	AND
		EM_WD_Priority_Display = 1
	AND
		EM_WD_Interface_ID = @EM_WD_Interface_ID
		
IF @@RowCount = 0
	SET @Error_Message = @Error_Message + ', Invalid Priority Code'


/* Check to see if Category Code is in database */		
SELECT 
		EM_WD_Category_Code
FROM
		EM_WD_Category
INNER JOIN
		EM_WD_Interface_Category_Lookup
	ON
			EM_WD_Interface_Category_Lookup.EM_WD_Category_ID = EM_WD_Category.EM_WD_Category_ID
WHERE 
		EM_WD_Category_Code = @EM_WD_Category_Code	
	AND
		EM_WD_Category_Display = 1	
	AND
		EM_WD_Interface_ID = @EM_WD_Interface_ID
IF @@RowCount = 0
	SET @Error_Message = @Error_Message + ', Invalid Category Code'


/* Check to see if Index Code is in database */
SELECT 
		EM_WD_Index_Code
FROM
		EM_WD_Index
INNER JOIN
		EM_WD_Interface_Index_Lookup
	ON
			EM_WD_Interface_Index_Lookup.EM_WD_Index_ID = EM_WD_Index.EM_WD_Index_ID
WHERE 
		EM_WD_Index_Code = @EM_WD_Index_Code
	AND
		EM_WD_Index_Display = 1	
	AND
		EM_WD_Interface_ID = @EM_WD_Interface_ID
IF @@RowCount = 0
	SET @Error_Message = @Error_Message + ', Invalid Index Code'


/* Check to see if filename is in database */
SELECT 
		Image_Filename
FROM
		Image_Main
WHERE 
		Image_Filename = @Image_Filename
IF @@RowCount > 0
	SET @Error_Message = @Error_Message + ', File Already In Database'


/* remove the ", " from the error message */
If @Error_Message != ''
SET @Error_Message = Right(@Error_Message, Len(@Error_Message)-2)


/* If an error was found do not run insert statement */
IF @Error_Message = ''
	BEGIN
		INSERT INTO
			Image_Main
				(Image_Filename,
				Image_Full_Path,
				Image_Directory,
				Image_Family_Count,
				Image_Family_Total_Count,
				Image_Created_Datetime,
				Image_Modified_Datetime,
				Image_Size,
				Image_Main_Add_Datetime)
			VALUES
				(@Image_Filename,
				@Image_Full_Path,
				@Image_Directory,
				@Image_Family_Count,
				@Image_Family_Total_Count,
				@Image_Created_Datetime,
				@Image_Modified_Datetime,
				@Image_Size,
				@Image_Add_Datetime)

		SET @Image_ID = CAST(SCOPE_IDENTITY() AS INT)

		INSERT INTO
			Image_Properties
				(Image_ID,
				EM_WD_Interface_ID,
				EM_WD_Priority_Code,
				EM_WD_Category_Code,
				EM_WD_Source_Code,
				EM_WD_Index_Code,
				Image_Family,
				Image_Oldest_Datetime,
				Image_Properties_Add_Datetime)
			VALUES
				(@Image_ID,
				@EM_WD_Interface_ID,
				@EM_WD_Priority_Code,
				@EM_WD_Category_Code,
				@EM_WD_Source_Code,
				@EM_WD_Index_Code,
				@Image_Family,
				@Image_Oldest_Datetime,
				getdate())

		INSERT INTO
			Image_Queues
				(Image_ID,
				Image_Queue_Type_ID,
				Image_Queue_Team_ID,
				Image_Queue_Users_ID,
				Image_Queues_Added_Datetime,
				Image_Queues_Added_Users_ID,
				Image_Queues_Assigned_Datetime,
				Image_Queues_Assigned_Users_ID
				)
			VALUES
				(@Image_ID,
				@Image_Queue_Type_ID,
				@Image_Queue_Team_ID,
				@Image_Queue_Users_ID,
				getdate(),
				@Image_Queues_Added_Users_ID,
				@Image_Queues_Assigned_Datetime,
				@Image_Queues_Assigned_Users_ID)

		SET @Image_Queues_ID = CAST(SCOPE_IDENTITY() AS INT)

		INSERT INTO
			Image_Status_Log
				(Image_ID,
				EM_WD_Users_ID,
				Image_Status_Datetime_Added,
				Image_Status_ID)
			VALUES
				(@Image_ID,
				@EM_WD_Users_ID,
				getdate(),
				@Image_Status_ID)
		-- Interface 1 (FA) uses Move_To_ERP
		-- Interface 2 (EM) uses Move to Banner_Nolij		
		IF @EM_WD_Interface_ID = '1'	
			BEGIN
				INSERT INTO
					Image_Move_To_ERP
						(Image_ID,
						Image_Move_To_ERP_Datetime_Added,
						Banner_ID,
						Original_Banner_ID)
					VALUES
						(@Image_ID,
						getdate(),
						@Banner_ID,
						@Banner_ID)
			END
		ELSE
			BEGIN
				INSERT INTO
					Image_Move_To_Nolij_Banner
						(Image_ID,
						Move_To_Nolij_Datetime_Added,
						Banner_ID,
						Original_Banner_ID)
					VALUES
						(@Image_ID,
						getdate(),
						@Banner_ID,
						@Banner_ID)
			END
			
		-- Check to see if this is a problem image
		-- If so then load into the problem queue	
		IF @Image_Problem_Type_ID > 0	
			BEGIN
				INSERT INTO
					Image_Problems
						(Image_Queues_ID,
						Image_ID,
						Image_Problem_Type_ID,
						Image_Problems_Comment,
						Image_Problems_Added_Datetime,
						Image_Problems_Added_Users_ID)
					VALUES
						(@Image_Queues_ID,
						@Image_ID,
						@Image_Problem_Type_ID,
						'Child of merged image',
						getdate(),
						@Image_Queues_Added_Users_ID)

					--Update the queue and mark the images as delivered
				UPDATE
					Image_Queues
				SET
					Image_Queues_Delivered_Datetime = getdate(),
					Image_Queues_Delivered_Users_ID = @Image_Queues_Added_Users_ID
				WHERE
					Image_ID = @Image_ID AND
					Image_Queue_Type_ID = @Image_Queue_Type_ID
			END
	
	END 
	

